
# Introduction

This repository contains simple example projects that I create in
the process of learning [JABM](http://jabm.sourceforge.net/) and
[JMAB](https://github.com/S120/jmab).

# Projects

There is no additional explanatory text (as of now). These
example projects are meant to be used as self-study.

## 01: HelloWorldSim

The equivalent of a "Hello World" program in an agent-based model
simulation.

A simple simulation environment is set up, some agents are
created and the post the obligatory "Hello World" message.
