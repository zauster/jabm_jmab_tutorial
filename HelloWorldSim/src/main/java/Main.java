
package SimpleEventTest;

import net.sourceforge.jabm.SimulationManager;

public class Main {
    public static void main(String [] args) {
        System.setProperty("jabm.config", "config/simpletest.xml");
        System.setProperty("jabm.propertyfile", "config/experiment.properties");
        SimulationManager.main(new String[] {});
    }
}
