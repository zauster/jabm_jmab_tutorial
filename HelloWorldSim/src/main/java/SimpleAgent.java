
package SimpleEventTest;

import java.io.Serializable;
import net.sourceforge.jabm.EventScheduler;
import net.sourceforge.jabm.agent.AbstractAgent;
import net.sourceforge.jabm.event.SimEvent;
import net.sourceforge.jabm.event.AgentArrivalEvent;
import net.sourceforge.jabm.event.SimulationFinishedEvent;
import net.sourceforge.jabm.event.SimulationStartingEvent;

public class SimpleAgent extends AbstractAgent implements Serializable {

    /**
     * "Hello World" Agent
     */

    // some member variables
    protected String someString;

    public SimpleAgent(EventScheduler scheduler) {
        super(scheduler);
    }

    public SimpleAgent() {
        super();
    }

    @Override
    public void onAgentArrival(AgentArrivalEvent event) {
        // super.onAgentArrival(event);
        System.out.println("Agent Arrival: " + someString);
    }

    @Override
    public void initialise() {
        System.out.println("Initialisation of the agent...");
    }

    public void setSomeString(String initString) {
        someString = initString;
    }

    @Override
    public double getPayoff() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public void eventOccurred(SimEvent event) {
        super.eventOccurred(event);
    }

    @Override
    public void subscribeToEvents() {
        super.subscribeToEvents();
    }

    @Override
    public String toString() {
        return "Some string: " + someString;
    }

}
