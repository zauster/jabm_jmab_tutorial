
# Simple "Hello World" Simulation using jmab

## Prerequisites

- gradle
- a recent JDK

## Usage

```
gradle build
gradle run
```
